const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ProductSchema = new Schema({
  name: {
    type: String,
    required: true,
    default: ''
  },
  price: {
    type: Number,
    min: 0,
    required: true
  },
  sellerId: {
    type: String,
    required: true
  }
}, {
  timestamps: true,
  toJSON: {
    transform: function (doc, ret, options) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.__v;
      return ret;
    }
  }
});

// ProductSchema.methods.decrementStock = function(cb) {};

ProductSchema.index({ name: 1, sellerId: 1 }, { unique: true });

module.exports = mongoose.model('Product', ProductSchema);
