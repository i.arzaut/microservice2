require('dotenv').config();
const mongoose = require('mongoose');
const cors = require('cors');
const express = require('express');
const app = express();
const admin = require("firebase-admin");
const corsOptions = require('./config/cors')
const test = require('./route/test');
const port = process.env.APP_PORT || 4009;
//CONNECT TO DB
const mongoUri =
  process.env.NODE_ENV == 'test'
    ? process.env.DB_URL_TEST
    : process.env.NODE_ENV == 'production'
      ? process.env.DB_URL_PROD
      : process.env.DB_URL_DEV

mongoose.connect(mongoUri);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

//CONNECT TO FIREBASE
admin.initializeApp({
  credential: admin.credential.cert('./simple-comercio-firebase-admin.json'),
  databaseURL: "https://simple-comercio.firebaseio.com"
});

//SETTINGS
app.use(cors(corsOptions));
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

//ROUTES
app.use('/api/test', test);

db.once('open', function () {
  console.log('Connected to database succesfully')
  app.listen(port, () => {
    console.log(`Service 2 listening on port ${port}`);
  });
});
