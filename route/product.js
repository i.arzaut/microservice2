const express = require('express');
const router = express.Router();
const fs = require('fs');
const Product = require('../db/models/product');
const { checkJwt, jwtAuthz } = require('../middlewares/jwt');
const RPCService = require('../integration/RPCService');



module.exports = router;
