const express = require('express');
const router = express.Router();
const fs = require('fs');
const Product = require('../db/models/product');
const { checkJwt, jwtAuthz } = require('../middlewares/jwt');
const RPCService = require('../integration/RPCService');



router.get('/', async (req, res, next) => {
  const rpcResponse = await RPCService.publishToQueue('test', { message: "Este es el mensaje" })
  if (rpcResponse.status !== 200)
    return res.status(rpcResponse.status).send({ details: rpcResponse.details })
  const testObject = rpcResponse.data
  console.log(testObject)
  return res.status(200).send(testObject)
})

module.exports = router;
